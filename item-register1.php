<?php
/**
 * @version		$Id: item.php 1709 2012-10-06 01:46:10Z joomlaworks $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<?php if(JRequest::getInt('print')==1): ?>
<!-- Print button at the top of the print page only -->
<a class="itemPrintThisPage" rel="nofollow" href="#" onclick="window.print();return false;">
	<span><?php echo JText::_('K2_PRINT_THIS_PAGE'); ?></span>
</a>
<?php endif; ?>

<!-- Start K2 Item Layout -->
<span id="startOfPageId<?php echo JRequest::getInt('id'); ?>"></span>

<div id="k2Container_1" class="itemView<?php echo ($this->item->featured) ? ' itemIsFeatured' : ''; ?><?php if($this->item->params->get('pageclass_sfx')) echo ' '.$this->item->params->get('pageclass_sfx'); ?>" style="margin-top:10px;">
<?php 
if($_GET["success"] == "true" && $_GET["payment"] == "bank"){
	?>
	
	<?php 
	
	//header("Refresh: 9; url=http://www..gr/");
} else {
	// display form
	if($_POST["seminar"] == NULL || $_POST["seminar"] == "" ||
		$_POST["surname"] == NULL || $_POST["surname"] == "" || 
		$_POST["name"] == NULL || $_POST["name"] == "" ||
		$_POST["fname"] == NULL || $_POST["fname"] == "" ||
		$_POST["cert"] == NULL || $_POST["cert"] == "" ||
		$_POST["address"] == NULL || $_POST["address"] == "" ||
		$_POST["city"] == NULL || $_POST["city"] == "" ||
		$_POST["pc"] == NULL || $_POST["pc"] == "" ||
		$_POST["fphone"] == NULL || $_POST["fphone"] == "" ||
		$_POST["mphone"] == NULL || $_POST["mphone"] == "" ||
		$_POST["email"] == NULL || $_POST["email"] == ""){
	?>
		
		<form name="form1" id="form1" action="http://www.iekemtee.gr/el/register-step2" enctype="multipart/form-data" 
								method="post" onsubmit="return validateForm();" style="padding:40px 0 20px 0;">
		<fieldset class="unit"  style="border:none; padding:0; margin:0;">
		<legend style="display:block; width:935px; height:60px;margin-left: -3px; background:url(images/seminars/enroll_head.png) no-repeat;"><span style="display:block;margin-left:90px; padding-top:18px; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; color:#5f6a72;">Επιλογή σεμιναρίου</span></legend>
		<div id="seminar" style="width:945px; margin-left:21px;">
			<?php 
			
			$db =JFactory::getDBO();
		 $query = $db->getQuery(true);
		
		// Set the query using our newly populated query object and execute it.
		$query = "SELECT `id`, `title` FROM `jos_k2_items` WHERE `catid`=1;";
		//$query->select('*');
		//$query->from('#__k2_items'); 
		//$query->where('catid = 1'); 
		$db->setQuery($query);
		//$db->query();
		$result = $db->loadObjectList();
		//var_dump($result);
			?>
            <div  style="width:945px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:16px; color:#333333;">
			<label for="seminar"><span class="required2"><sup></sup></span></label><br />
			
			    <?php foreach($result as $obj){ ?>
                <span style="display:inline-block;width:283px; height:56px; background:url(images/seminars/seminar_tab<?php if($_GET["mid"]==$obj->id) echo '2'; ?>.png) no-repeat; margin:0 20px 10px 0;">
                <input type="checkbox" name="seminar[]" class="styled-checkbox" <?php if($_GET["mid"]==$obj->id) echo 'checked="yes"'; ?> id="seminar<?php echo $obj->id;?>"  value="<?php echo $obj->title;?>" style="margin:15px 20px 0 18px;" onclick="calculateTotal()" /><label class="checkbox-label" id="label<?php echo $obj->id;?>" style="text-align: center;width: 200px;display: inline-block; position:relative; top:-9px; left: -14px;<?php if($_GET["mid"]==$obj->id) echo 'color:#fff;'; ?>"><?php echo $obj->title;?></label>
                
                </span>
				
				<?php } ?>
			</div>
			
		</div>
        <div id="hidprice"><input type="hidden" class="textfield" name="price" id="price" value="<?php if (!isset($_GET["mid"])) echo ""; elseif($_GET["mid"]==4) echo "2200,00"; elseif($_GET["mid"]==17) echo "2800,00"; else echo "270,00"; ?>" /></div>
         		<div id="totalPrice1" style="width:872px;margin:0 auto; text-align:right; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:16px; color:#0086c5; font-weight:bold; margin-bottom:20px;">
         <?php if (!isset($_GET["mid"])) echo ""; else echo "Έχετε επιλέξει ένα σεμινάριο";  ?>
         </div>

         <div id="info" style="width:889px; margin:0 auto; padding:20px 0; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:12px; color:#696969;">Παρακαλούμε συμπληρώστε τα πεδία της φόρμας χρησιμοποιώντας Ελληνικούς χαρακτήρες. Τα πεδία μαρκαρισμένα με αστερίσκο (<span class="required2">*</span>) είναι υποχρεωτικά.
		</div>
		</fieldset>
        
        
		<fieldset class="unit" style="border:none;padding:0; margin:0;">
        <legend style="display:block; width:935px; height:60px;margin-left: -3px; background:url(images/seminars/enroll_head.png) no-repeat;"><span style="display:block;margin-left:90px; padding-top:18px; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; color:#5f6a72;">Προσωπικά στοιχεία</span></legend>
		
        <div style="width:889px; margin:0 auto; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:14px; color:#414141;">
		<div id="leftcol" style="display:inline-block; width:389px; float:left;margin-top: 20px;margin-left: 20px;">
			
			<label for="surname">Επώνυμο <span class="required2"><sup>*</sup></span></label><br />
			<input type="text" class="textfield" name="surname" id="surname" size="25" tabindex="3" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" /><br />
			
			<label for="address">Διεύθυνση <span class="required2"><sup>*</sup></span></label><br />
			<input type="text" class="textfield" name="address" id="address" size="25" tabindex="7" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" /><br />
			
			<label for="pc">Ταχυδρομικός Κώδικας <span class="required2"><sup>*</sup></span></label><br />
			<input type="text" class="textfield" name="pc" id="pc" size="25" tabindex="9" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" /><br />
			
			<label for="mphone">Κινητό Τηλέφωνο <span class="required2"><sup>*</sup></span></label><br />
			<input type="text" class="textfield" name="mphone" size="25" id="mphone" tabindex="11" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" />
            
            <label for="category">Κατηγορία εκπαιδευόμενου <span class="required2"><sup>*</sup></span></label><br />
            <select id="category" name="category" onchange="text(this);calculateTotal();" style="width:392px; height:24px; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1); line-height:1.5; ">
  				<option selected="selected" disabled="disabled" value="">επιλέξτε κατηγορία</option>
                <option value="student" style="height:15px;">φοιτητής</option>
                <option value="alumni" style="height:15px;">απόφοιτος των σεμιναρίων μας</option>
                <option value="unemployed" style="height:15px;">εισόδημα κάτω του αφορολόγητου ή κάτοχος κάρτας ανεργίας</option>
                <option value="none" style="height:15px;">καμία από τις παραπάνω</option>  				
 			</select>

		</div>
		<div id="rightcol" style="display:inline-block; width:389px; float:right;margin-top: 20px;margin-right: 20px;">
			
			<label for="name">Όνομα <span class="required2"><sup>*</sup></span></label><br />
			<input type="text" class="textfield" name="name" id="name" size="25" tabindex="4" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" /><br />
			
			<label for="city">Πόλη <span class="required2"><sup>*</sup></span></label><br />
			<input type="text" class="textfield" name="city" id="city" size="25" tabindex="8" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" /><br />
			
			<label for="fphone">Σταθερό Τηλέφωνο <span class="required2"><sup>*</sup></span></label><br />
			<input type="text" class="textfield" name="fphone" id="fphone" size="25" tabindex="10" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" /><br />
			
			<label for="email">Email <span class="required2"><sup>*</sup></span></label><br />
			<input type="text" class="textfield" name="email" id="email" size="25" tabindex="12" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" />
		</div>
        </div>
        
		</fieldset>
		<div id="message" style="width:850px; margin:10px auto; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:13px; color:#414141; text-align:justify;"></div>		
		<br />
		<div id="totalPrice2" style="width:872px;margin:0 auto; text-align:right; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:16px; color:#0086c5; font-weight:bold; margin-bottom:20px;">
         <?php if (!isset($_GET["mid"])) echo ""; elseif($_GET["mid"]==4) echo "Συνολικό κόστος 1700 Ευρώ"; elseif($_GET["mid"]==17) echo "Συνολικό κόστος 2800 Ευρώ"; else echo "Συνολικό κόστος 330 Ευρώ"; ?>
         </div>
		
		
		<div id="buttons" style="text-align:center;">
		
		<input type="reset" class="cbutton" value="Καθαρισμός" tabindex="28" style="width:183px; height:41px; background:url(images/seminars/payment.png) no-repeat; border:none; cursor:pointer; font-family:'Century Gothic'; font-size:16px; font-weight:bold; color:#666666;"/>
        <input type="submit" class="cbutton" value="Επόμενο" tabindex="27" style="width:183px; height:41px; background:url(images/seminars/payment.png) no-repeat; border:none; cursor:pointer; font-family:'Century Gothic'; font-size:16px; font-weight:bold; color:#666666;"/>
		</div>
        
		</form>
        
         <div id="contact_1" style="width:940px; height:186px; margin-left: -2px; margin-top:10px; background:url(images/seminars/footer.png);">
    	<div style="width:50%; float:left; margin-top:58px; margin-left:21px;">
       <span style="display:inline-block;"><a onclick="window.open('https://services.yuboto.com/click2call/new/c2t.aspx?id=6B87B610-D272-4E1E-BD32-B961B6573D2E','Image','scrollbars=no,toolbar=no,location=no,status=no,resizable=no,screenX=136,screenY=83');return false;" href="https://services.yuboto.com/click2call/new/c2t.aspx?id=6B87B610-D272-4E1E-BD32-B961B6573D2E" class="TT-container">
       <img src="images/seminars/click2call_1.png" border="0" onmouseover="this.src='images/seminars/click2call_2.png'" onmouseout="this.src='images/seminars/click2call_1.png'">
	<span class="TT-value"></span></a></span>
    <span style="display:inline-block; margin-left:16px; position:relative; top:-13px;">
    	<span style="color:#67348e; font-size:14px; font-weight:bold;"></span>
        <br />
        <span style="width:259px; color:#555555; font-size:14px; font-weight:bold; position:relative; top:-5px;">Για να συνομιλήσετε μαζί μας χωρίς χρέωση<br />άμεσα, πατήστε εδώ.</span>
	</span>
    
        </div>
        
        <div style="display:inline-block; float:right; margin-top:52px; margin-right:21px;"">
        	<span style="display:block;float:right; font-style:italic; color:#262626; font-size:14px;">
            Για περισσότερες πληροφορίες σχετικά με τα σεμινάρια μας:
            </span>
            <br /><br />
            <span style="margin-top:20px;">
             <span style="display:inline-block; float:right; ">
            <a href="http://www.youtube.com/user/elearnDigitalAcademy" target="_blank">
            <img src="images/seminars/youtube.png" />
            </a>
            <a href="https://www.facebook.com/dga.gr" target="_blank">
            <img src="images/seminars/facebook.png" />
            </a>
            </span>
            <span style="display:inline-block;float:right; font-weight:bold; color:#0086c5; font-size:18px; margin-right:20px;margin-top: 5px; text-align:right;">
            τηλ: 210 3003536<br />Email: <a href="mailto:iekemtee@dga.gr" target="_blank" style="font-weight:bold; color:#0086c5; font-size:18px; text-decoration:none;">iekemtee@dga.gr</a>

            </span>
           </span>
        </div>
    </div>

        
		<?php 
	} else {
		
		?>
		<div id="error">
		<?php 
		$sendTo = trim($_POST["email"]);
		//check if the email address is invalid
		
		if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $sendTo)) {
		    die("<br /><br />To email που δώσατε δεν είναι έγκυρο.<br /><br />");
		}
		
		
		if (isset($_POST['seminar'])) {
    		$optionArray = $_POST['seminar'];
			$number =count($optionArray);
			if($number>1){
				$seminar=$optionArray[0];
    			for ($i=1; $i<$number; $i++) {
				   	$seminar .=', '. $optionArray[$i];
    				}
				}
		}else $seminar=$optionArray[0];
		//$seminar = $_POST["seminar"];
		
		// material language
		
		
		// education level
		
	
		// english level
		
		
		
		
		// create new record for registered candidate
		
		

		$candidate = array("seminar" => $seminar,
							"language" => $language,
							"surname" => $_POST["surname"],
							"name" => $_POST["name"],
							"fname" => $_POST["fname"],
							"cert" => $_POST["cert"],
							"address" => $_POST["address"],
							"city" => $_POST["city"],
							"pc" => $_POST["pc"],
							"fphone" => $_POST["fphone"],
							"mphone" => $_POST["mphone"],
							"email" => $sendTo,
							"company" => $_POST["company"],
							"activity" => $_POST["activity"],
							"vat" => $_POST["vat"],
							"doy" => $_POST["doy"],
							"caddress" => $_POST["caddress"]);
		$payment=$_POST["payment"];
		// upload resume
		//uploadFile();
		
		// send email to medialab
		//sendMailToMedialab($candidate);
		
		// send email to candidate
		//sendMailToCandidate($surname, $seminar, $sendTo);
		//header("Location: index.php/register-step2?success=true&payment=$payment");	
		
		
		?>
		</div>
		<?php 	
	}
}

?>

	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
<!--[if lt IE 9]><script>
// JS for IE support
// [Remove this if you're not supporting IE 8 and below]
$(function() {
    // IE 8 doesn't support :checked, so give it the 'checked' class for our
    // styles to work
    $('.lt-ie9 .styled-checkbox').click(function() {
        var $this = $(this);
        if($this.is(':checked')) {
            $this.addClass('checked');
        }
        else {
            $this.removeClass('checked');
        }
    });

    // IE 6 & 7 don't support `:before`, so manually add a span with the same
    // styling and update the content on change
    // [Remove this if you're not supporting IE 6 & 7]
    $('.lt-ie8 .styled-checkbox').each(function() {
        $(this).before('<span class="checkbox-replacement"></span>');
    });
    $('.lt-ie8 .styled-checkbox').click(function() {
        var $this = $(this);
        if($this.is(':checked')) {
            $this.siblings('.checkbox-replacement').html(' ');
        }
        else {
            $this.siblings('.checkbox-replacement').html('');
        }
    });

    // Trigger now to set initial state
    $('.lt-ie9 form .styled-checkbox').change();
});
</script><![endif]-->