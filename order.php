<?php

include('config.php');//for recaptcha config
include('helper.php');
include('recaptcha/recaptchalib.php');

session_start();

$db = get_db();

//get user input if exists in session
if(isset($_SESSION['user_input'])) {
    $user_input = $_SESSION['user_input'];
    unset($_SESSION['user_input']);
}

if(isset($_SESSION['recaptcha_error'])) {
    $recaptcha_error = $_SESSION['recaptcha_error'];
    unset($_SESSION['recaptcha_error']);
}


$lessons = get_dga_lessons($db);
if($lessons == NULL) {
    die('no lessons');
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="css/style.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="../web/css/template_css.css"/>
        <title>Digital Academy</title>
        <script type="text/javascript" src="js/jquery-1.6.1.js"></script>
        <script language="javascript" src="js/validation.js"></script>
        <script type="text/javascript">
            var RecaptchaOptions = {
               theme : 'white'
            };
        </script>
    </head>

    <body>
    <div align="center">
   
    <div class="menu_div" style="margin-top:30px;">
        <table>
        <tr>
            <td class="menu_item"><a href="../web/intro/index.php" class="menu_link">HOME</a></td><td class="small_gap"></td>
                <td class="menu_item"><a href="../web/msg/index.html" class="menu_link">ΕΙΣΑΓΩΓΗ</a></td><td class="small_gap"></td>
            <td class="menu_item"><a href="../web/info/index.html" class="menu_link">ΠΛΗΡΟΦΟΡΙΕΣ</a></td><td class="small_gap"></td>
            <td class="menu_item"><a href="../web/seminars/index.php" class="menu_link">ΣΕΜΙΝΑΡΙΑ</a></td><td class="small_gap"></td>
            <td class="menu_item"><a href="../web/staff/index.php" class="menu_link">ΠΡΟΣΩΠΙΚΟ</a></td><td class="small_gap"></td>
            <td class="menu_item"><a href="../web/faq/index.php" class="menu_link">FAQ</a></td><td class="small_gap"></td>
            <td class="menu_item"><a href="../registration/form.php" target="_self" class="menu_link">ΕΓΓΡΑΦΕΣ</a></td><td class="small_gap"></td>
            <td class="menu_item"><a href="../web/publications/index.php" class="menu_link">ΕΚΔΟΣΕΙΣ</a></td><td class="small_gap"></td>
            <td class="menu_item"><a href="../web/contact/index.html" class="menu_link">ΕΠΙΚΟΙΝΩΝΙΑ</a></td><td class="small_gap"></td>
            <td class="menu_item"><a href="order.php" class="menu_link">ΠΛΗΡΩΜΗ</a></td><td class="small_gap"></td>
       </tr>
    </table>
    </div>

    <div class="form" align="right">
    <div id="policy">
   <p class="bold">Όροι Χρήσης:</p>
		<p style="text-align: justify;">	Από την στιγμή της καταβολής των διδάκτρων, τα χρήματα <span class="bold">ΔΕΝ ΕΠΙΣΤΡΕΦΟΝΤΑΙ</span>,  
		εκτός της περίπτωσης πλήρους αναβολής των μαθημάτων, με ευθύνη της Digital Academy. Οποιοσδήποτε χρήστης αδυνατεί
			να παρακολουθήσει το σεμινάριο λόγω <span class="bold">ΣΟΒΑΡΟΥ</span> κωλύματος ενώ έχει καταβάλλει 
			τα δίδακτρα, δύναται μέσω επικοινωνίας με τους υπεύθυνους του προγράμματος να μεταφερθεί σε επόμενη 
			περίοδο διδασκαλίας, έτσι ώστε να ολοκληρώσει το πρόγραμμα εκπαίδευσης.
</p> 
    
    <div id="return">Επιστροφή</div>
    </div>
    
    
    	<div id="form_fields">
        <form name="order" id="order" action="pay.php" method="post"><!--validation -->
            <div id="logo" >
               <a href="http://dga.gr" target="_blank"> <img src="images/medialab_logo.png" border="0"/></a>
            </div>
            <div class="container"><span>Όνομα :</span> <span class="asterisk" >*</span>
                <input id="first_name" class="form_field" type="text" name="first_name" value="<?php echo (isset($user_input))?$user_input['first_name']:''; ?>"/>
            </div>
            <div class="container"><span>Επίθετο :</span> <span class="asterisk" >*</span>
                <input id="last_name" class="form_field" type="text" name="last_name" value="<?php echo (isset($user_input))?$user_input['last_name']:''; ?>"/>
            </div>
            <div class="container"><span>Κινητό Τηλέφωνο :</span> <span class="asterisk" >*</span>
                <input id="phone_number" class="form_field" type="text" name="phone_number" value="<?php echo (isset($user_input))?$user_input['phone_number']:''; ?>"/>
            </div>
            <div class="container"><span>e-mail :</span> <span class="asterisk" >*</span>
                <input id="email" class="form_field" type="text" name="email" value="<?php echo (isset($user_input))?$user_input['email']:''; ?>"/>
            </div>
            
            <div id="border_cont">
                <div class="lesson">
                    <div class="title"><span>Επιλογή μαθήματος :</span> <span class="asterisk" >*</span></div> 
                        <select id="lesson" name="lesson">
                            <option value="0" <?php echo (isset($user_input))?'':'selected'; ?>>Επιλέξτε ένα σεμινάριο</option>
                            <?php 
                                while($lesson = $lessons->fetch_assoc()){
                            ?>                            
                            <option name="lesson" value="<?php echo $lesson['id']; ?>" title="<?php echo $lesson['max_installments'] ?>" <?php echo ((isset($user_input)) && ($user_input['lesson']==$lesson['id']))?'selected':''; ?>><?php echo $lesson['lesson_name']; ?> </option>
                            <?php
                                }
                                $db->close();
                            ?>
                        </select>
                </div>
                        
                    <div id="installments-container" align="center">
                       <div  class="title"><span>Δόσεις :</span></div>
                       <select id="installments" name="installments">
                           <?php if(isset($user_input)) { ?>
                           <option value="<?php echo $user_input['installments']; ?>" selected> <?php echo $user_input['installments']; ?> Δόσεις </option>
                           <?php } else { ?>
                           <option value="1" selected>Χωρίς Δόσεις</option>
                           <?php } ?>
                        </select>
                    </div>
                    
                    <div class="clearer"></div>
                            </div>
            
            <div id="acceptance" style="margin: 15px;">
			<p style="font-size:12px;float:left;margin:0px;">Δηλώνω υπεύθυνα πως τα στοιχεία που υποβάλλω είναι αληθή και αποδέχομαι 
			πλήρως τους <spam class="read_policy">όρους χρήσης</spam>:</p>
			<input style="width:12px;height:15px;margin-top:0px;" type="checkbox" name="accept" id="accept">
		</div>
            
            <div style="text-align:center">
            <div id="recaptcha" >
                <?php
                    if(isset($recaptcha_error)) {
                        echo recaptcha_get_html($publickey, $recaptcha_error);
                    } else {
                        echo recaptcha_get_html($publickey);
                    }
                ?>
            </div>
                </div>
            <div id="order_btn" align="center">
                <input value="Πληρωμή" type="submit"  class="button"/>
            </div>
                <div class="clearer"></div>
            
            
            
            <div id="images">
            <a href="http://www.alpha.gr/e-Commerce"  target="_blank"><img src="images/horizontal%20banner.jpg"/></a>
            </div>
            
            
            
            <div class="clearer"></div>
    
    
        </form>
        </div>
        </div>
        
       
        </div>
    </body>
</html>
