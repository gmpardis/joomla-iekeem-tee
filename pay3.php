<?php
include('config.php'); //for recaptcha and paycenter config 
include('helper.php');
//include('recaptcha/recaptchalib.php');
session_start();

//read user input
$firstnamepay = $_POST['first_namepay'];
$lastname = $_POST['last_name'];
$phone_number = $_POST['phone_number'];
$email = $_POST['email'];
$lesson = $_POST['lessonsname'];
$installments = $_POST['installments'];
$charging = $_POST['charging'];
//test user input
//add input to session
$user_input = array(
    'first_namepay' => $firstnamepay,
    'last_name' => $lastname,
    'phone_number' => $phone_number,
    'email' => $email,
    'lesson' => $lesson,
    'installments' => $installments,
    'charging' => $charging
);

$_SESSION['user_input'] = $user_input;


$db = get_db();

/*
 * save to db info and get reference merchant
 */
if (!( isset($_POST['first_namepay']) && isset($_POST['last_name']) && isset($_POST['phone_number']) && isset($_POST['email']) && isset($_POST['lessonsname']) )) {
    die('not enough info');
}


if (($customer_id = get_customer_id($firstname, $lastname, $phone_number, $email, $db)) == 0) {
    die('did not get customer id');
}

if (($merchant_reference = get_order_id($customer_id, $lesson['id'], $lesson['cost'], $installments, $db) ) == 0) {
    die('did not get order id');
}

/* order params */
//$amount = number_format($lesson['cost'], 2, '.', '');

$parameters = ""; //parameters passed to success/fail pages

//if (isset($_GET['act']) && $_GET['act'] == "send") {
//var_dump($_POST);            die(1);
    //var_dump($_POST);
   // var_dump($amount." - ".$merchant_reference);
    $form_data = "";
    $form_data_array = array();
    $currency = "EUR";
//$form_mid = "0022442502";
    $form_mid = "0020868261";
    $form_data_array[1] = $form_mid;     //Req
    $form_lang = "el";//$_POST['lang'];
    $form_data_array[2] = $form_lang;     //Opt
    $form_order_id = $merchant_reference;//$_POST['orderid'];
    $form_data_array[3] = $form_order_id;    //Req
    $form_order_desc = $lesson; //$_POST['orderDescr'];
    $form_data_array[4] = $form_order_desc;    //Opt
    $form_order_amount = $charging; //$_POST['orderAmount'];
    $form_data_array[5] = $form_order_amount;   //Req
    $form_currency = $currency; //$_POST['currency'];
    $form_data_array[6] = $form_currency;    //Req
    $form_email = $_POST['email'];
    $form_data_array[7] = $form_email;     //Req
    $form_phone = $phone_number;//$_POST['phonenumber'];
    $form_data_array[8] = $form_phone;     //Opt
    if($installments > 1){ 
    	$form_ext_install_offset='0';
    	$form_data_array[9]=$form_ext_install_offset;
    	$form_ext_install_period = $installments;
    	$form_data_array[10] = $form_ext_install_period; //Opt */
    }

//$form_cssurl = $_POST['cssUrl'];
//$form_data_array[32] = $form_cssurl;    //Opt

    $form_confirm_url = "http://dga.gr/paycenter/success.php"; //$_POST['confirmUrl'];
    $form_data_array[11] = $form_confirm_url;   //Req
    $form_cancel_url = "http://dga.gr/paycenter/fail.php"; //$_POST['cancelUrl'];
    $form_data_array[12] = $form_cancel_url;   //Req
    $form_secret = "p2QKbzMEFtCwrJIoIJ1h2t";

    $form_data_array[13] = $form_secret;    //Req

    $form_data = implode("", $form_data_array);

    $digest = base64_encode(sha1($form_data,true));
    
    $send_it_2 = "https://www.alphaecommerce.gr/vpos/shophandlermpi";
    $db->close();
    ?>

    <!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="stylesheet" href="css/style.css" type="text/css" />
            <title>Digital Academy</title>
            <script type="text/javascript" src="js/jquery-1.6.1.js"></script>
            <script language="javascript" src="js/validation.js"></script>
            <script type="text/javascript">
                function send_tran() {
                    var frm = document.getElementById('shopform1');
                    frm.style.visibility = "hidden";
                    frm.submit();
                }

            </script>
        </head>

        <body>
            <div align="center">
                <div class="form" align="right">






                    <div id="form_fields">
                        <form id="shopform1" name="form_submit" action="https://www.alphaecommerce.gr/vpos/shophandlermpi" method="post">
                            <div id="logo" class="pay_logo">
                                <a href="http://dga.gr" target="_blank"> <img src="images/medialab_logo.png" border="0"/></a>
                            </div>
                            <div class="entry"> Όνομα : <span><?php echo $firstnamepay; ?></span></div>
                            <div class="entry"> Επώνυμο :<span> <?php echo $lastname; ?></span></div>
                            <div class="entry"> Τηλέφωνο : <span><?php echo $phone_number; ?></span></div>
                            <div class="entry"> e-mail :<span> <?php echo $email; ?></span></div>
                            <div class="entry"> Σεμινάριο : <span><?php echo $lesson; ?></span></div>
                            <div class="entry"> Τιμή :<span> <?php
                                    echo $charging;
                                    echo ($installments > 1) ? " σε $installments άτοκες δόσεις" : "";
                                    ?></span></div>
                            <div class="entry">    Επιλογή γλώσσας σελίδας πληρωμής :
                                <select id="LanguageCode" name="LanguageCode">
                                    <option name="LanguageCode" value="el-GR" selected> Ελληνικά</option>
                                    <option name="LanguageCode" value="en-US" > English</option>
                                </select>
                            </div>
                            <input type="hidden" name="mid" value="<?php echo $form_mid ?>"/>
                            <input type="hidden" name="lang" value="<?php echo $form_lang ?>"/>
                            <input type="hidden" name="orderid" value="<?php echo $form_order_id ?>"/>
                            <input type="hidden" name="orderDesc" value="<?php echo $form_order_desc ?>"/>
                            <input type="hidden" name="orderAmount" value="<?php echo $form_order_amount ?>"/>
                            <input type="hidden" name="currency" value="<?php echo $form_currency ?>"/>
                            <input type="hidden" name="payerEmail" value="<?php echo $form_email ?>"/>
                            <input type="hidden" name="payerPhone" value="<?php echo $form_phone ?>"/>
                            <?php if($installments > 1): ?>
		            <input type="hidden" name="extInstallmentoffset" value="<?php echo $form_ext_install_offset ?>"/>
			    <input type="hidden" name="extInstallmentperiod" value="<?php echo $form_ext_install_period ?>"/>
			    <?php endif ?>
                            <input type="hidden" name="confirmUrl" value="<?php echo $form_confirm_url ?>"/>
                            <input type="hidden" name="cancelUrl" value="<?php echo $form_cancel_url ?>"/>
                            <input type="hidden" name="digest" value="<?php echo $digest ?>">
<!--
                            <input type="hidden" name="AcquirerId" value="<?php echo $acquirer_id; ?>" />
                            <input type="hidden" name="MerchantId" value="<?php echo $merchant_id; ?>" />
                            <input type="hidden" name="PosId" value="<?php echo $pos_id; ?>" />
                            <input type="hidden" name="User" value="<?php echo $username; ?>" />
                            <input type="hidden" name="MerchantReference" value="<?php echo $merchant_reference; ?>" />
                            <input type="hidden" name="ParamBackLink" value="" />

-->

                            <div id="order_btn" align="center">
                                <input value="Συνέχεια" type="submit"  class="button"/>
                            </div>
                            <div class="clearer"></div>
                            <!--<div id="images">
                                <div> <a href="http://www.piraeusbank.gr"  target="_blank"><img src="icons/PiraeusBank/PiraeusLogo_gr.gif" /></a></div>
                                <div class="sec_img">  <a href="icons/VbV/DescriptionPage/service_popup.htm"   target="_blank"><img src="icons/VbV/vbv.gif" /></a></div>
                                <div>   <a href="http://www.mastercardbusiness.com/mcbiz/index.jsp?template=/orphans&content=securecodepopup"   target="_blank" ><img src="icons/SecureCode/sc_learn_62x34.gif" /></a></div>
                            </div>-->
                            <div class="footer" style="text-align: center;margin-top:20px;">
                              <!--<?php include('cards.php'); ?>-->
				<a href="http://www.alpha.gr/e-Commerce"  target="_blank"><img src="images/horizontal%20banner.jpg"/></a>
                            </div>
                            <div class="clearer"></div>


                        </form>
                    </div>
                </div>
            </div>
        </body>
    </html>


    <!--<form id="shopform1" name="demo" method="POST" action="<?php echo $send_it_2 ?>" accept-charset="UTF-8" >

    </form>-->		

    </body>
    </html>		


