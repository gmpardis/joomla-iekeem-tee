<?php

/**

 * @version		2011-03-31 

 * @package		Joomla.Site

 * @copyright	Copyright (C) 2011 Simplify Your Web, Inc. All rights reserved.

 * @license		GNU General Public License version 3 or later; see LICENSE.txt

 */



defined('_JEXEC') or die;



/* The following line loads the MooTools JavaScript Library */

JHtml::_('behavior.framework', true);



/* get parameters */

$logo			= $this->params->get('logo');

$app			= JFactory::getApplication();

$templateparams	= $app->getTemplate(true)->params;


?>



<?php echo '<?'; ?>xml version="1.0" encoding="<?php echo $this->_charset ?>"?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >

	<head>

		<!-- The following JDOC Head tag loads all the header and meta information from the site config and content. -->

		<jdoc:include type="head" />



		<!-- The following line loads the template CSS file located in the template folder. -->

		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/base.css" type="text/css" media="screen" />

		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/joomla.css" type="text/css" media="screen" />

		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/screen.css" type="text/css" media="screen" />

		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/color.css" type="text/css" media="screen" />

		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" media="screen" />

		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/extensions.css" type="text/css" media="screen" />

		<?php if($this->direction == 'rtl') : ?>

			<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template_rtl.css" type="text/css" />

		<?php endif; ?>

		<!--[if lte IE 8]>

			<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template_ie.css" type="text/css" />

		<![endif]-->		

		

		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/print.css" type="text/css" media="print" />

		

		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/fonts/fonts.css" type="text/css" />

		

		<?php if((int)$this->params->get('templatewidth')>0) : ?>

			<style type="text/css">

				#template_framework {

					width: <?php echo (int)$this->params->get('templatewidth');?>px;

					margin-left: -<?php echo (int)$this->params->get('templatewidth')/2;?>px;

				}

			</style>

		<?php endif; ?>

		

		<!-- The following line loads the template JavaScript file located in the template folder. -->

		<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/template.js"></script>

		<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/modernizer.custom.js"></script>

		

		<script type="text/javascript">

			var frameworkWidth ='<?php echo (int)$this->params->get('templatewidth');?>px';

		</script>
         <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/html/com_k2/register/script.js"></script>
        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/formcalculations.js"></script>
        <!--[if !IE]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
        <!--[if !IE]><!--><script>
if (/*@cc_on!@*/false) {
    document.documentElement.className+=' ie10';
}
</script><!--<![endif]-->
        <script type='text/javascript'>//<![CDATA[ 

function check(elem) {
    
	var payment = document.getElementById("payment");
	var selection = payment.options[payment.selectedIndex].value;
	if(selection == "credit"){
		document.getElementById('installments').disabled = false;
		document.getElementById('doseis').innerHTML ='Αριθμός Δόσεων';
		document.form2.action ="http://www.dga.gr/paycenter/pay3.php";} //https://www.deltapay.gr/entry.asp https://www.deltapay.gr/getguid.asp
	else{
		document.getElementById('installments').disabled = true;
		document.getElementById('doseis').innerHTML =' ------ ';
		document.form2.action =window.location.href;
	}
}
function text(elem) {
	var e = document.getElementById("category");
	var categ_sel = e.options[e.selectedIndex].value;
	if (categ_sel == "student"){
	document.getElementById("message").innerHTML="Έχετε επιλέξει την κατηγορία “φοιτητής”. Εντός 24 ωρών θα επικοινωνήσουμε μαζί σας προκειμένου να σας ενημερώσουμε για τα απαραίτητα δικαιολογητικά που πρέπει να μας αποστείλετε. Συγκεκριμένα θα χρειαστεί αντίγραφο της φοιτητικής σας ταυτότητας ή οποιοδήποτε άλλο έγγραφο πιστοποιεί την φοιτητική σας ιδιότητα.  Στα δίδακτρά σας έχει ήδη εφαρμοστεί έκπτωση 15%. Μπορείτε να συνεχίσετε τη συναλλαγή σας.";
	}
	else if (categ_sel == "alumni"){
	document.getElementById("message").innerHTML="Έχετε επιλέξει την κατηγορία “απόφοιτος των σεμιναρίων μας”. Εντός 24 ωρών θα επικοινωνήσουμε μαζί σας προκειμένου να σας ενημερώσουμε για τα απαραίτητα δικαιολογητικά που πρέπει να μας αποστείλετε. Στα δίδακτρά σας έχει ήδη εφαρμοστεί έκπτωση 15%. Μπορείτε να συνεχίσετε τη συναλλαγή σας.";
	}
	else if (categ_sel == "unemployed"){
		document.getElementById("message").innerHTML="Έχετε επιλέξει την κατηγορία “Εισόδημα κάτω του αφορολόγητου ή κάτοχος κάρτας  ανεργίας”. Εντός 24 ωρών θα επικοινωνήσουμε μαζί σας προκειμένου να σας ενημερώσουμε για τα απαραίτητα δικαιολογητικά που πρέπει να μας αποστείλετε. Συγκεκριμένα θα χρειαστεί αντίγραφο της φορολογικής σας δήλωσης ή αντίγραφο της κάρτας ανεργίας σας.  Στα δίδακτρά σας έχει ήδη εφαρμοστεί έκπτωση 15%. Μπορείτε να συνεχίσετε τη συναλλαγή σας.";
	}
	else {
		document.getElementById("message").innerHTML="";
	}
}
//]]>  

</script>
<script type='text/javascript'>
 function test()
{
 if(document.getElementById('k2Container_1')){
	 
document.getElementById('k2Container_1').parentNode.parentNode.className += 'ecampus';}
}
window.onload=function(){
 test();
}
</script>

	</head>

	<body>

		<div id="template_framework">

			<div id="template_topmenu">

				<?php if($this->countModules('template-topmenu')) : ?>

					<jdoc:include type="modules" name="template-topmenu" style="xhtml" />

				<?php endif; ?>					

				<?php if($this->countModules('template-search')) : ?>

						<div id="template_search">

							<jdoc:include type="modules" name="template-search" style="none" />

						</div>

				<?php endif; ?>

			</div>		

			<div id="template_header">

				<div id="template_logo">					

					<?php if ($logo != null ): ?>						

						<a href="http://www.iekemtee.gr" id="logo" title="<?php echo htmlspecialchars($templateparams->get('sitetitle'));?>">

							<img src="<?php echo $this->baseurl ?>/<?php echo htmlspecialchars($logo); ?>" alt="<?php echo htmlspecialchars($templateparams->get('sitetitle'));?>" />

						</a>

						<h1 style="display:none"><?php echo htmlspecialchars($templateparams->get('sitetitle'));?></h1>

					<?php else: ?>

						<h1><?php echo htmlspecialchars($templateparams->get('sitetitle'));?></h1>

					<?php endif; ?>					

				</div>

					<?php if($this->countModules('template-theme')) : ?>						

						<div id="template_theme">

							<jdoc:include type="modules" name="template-theme" style="xhtml" />

						</div>

					<?php endif; ?>	

					<?php if($this->countModules('template-bellow-theme')) : ?>						

						<div id="template_bellow-theme">

							<jdoc:include type="modules" name="template-bellow-theme" style="xhtml" />

						</div>

					<?php endif; ?>	 

				<div id="template_mainmenubar">

					<?php if($this->countModules('template-mainmenu')) : ?>						

						<div id="template_mainmenu">

							<jdoc:include type="modules" name="template-mainmenu" style="xhtml" />

						</div>

					<?php endif; ?>

					<?php if($this->countModules('template-teemenu')) : ?>

						<div id="template_teemenu">

							<jdoc:include type="modules" name="template-teemenu" style="xhtml" />

							</div>

					<?php endif; ?>				

				</div>	

			</div>

				<?php if($this->countModules('template-tripletleft-top') 

					or $this->countModules('template-tripletmiddle-top') 

					or $this->countModules('template-tripletright-top')) : ?>	

				

					<div class="template_spacer"></div>					

					<div id="template_triplet-top">

						<div class="tripletleft">

							<?php if($this->countModules('template-tripletleft-top')) : ?>

								<jdoc:include type="modules" name="template-tripletleft-top" style="tripletmodule" />

				        	<?php endif; ?>

				       	</div>

				       	<div class="tripletmiddle">

							<?php if($this->countModules('template-tripletmiddle-top')) : ?>

								<jdoc:include type="modules" name="template-tripletmiddle-top" style="tripletmodule" />

				        	<?php endif; ?>	

				        </div>			    		        

						<div class="tripletright">

					        <?php if($this->countModules('template-tripletright-top')) : ?>

								<jdoc:include type="modules" name="template-tripletright-top" style="tripletmodule" />

					        <?php endif; ?>

				        </div>

					</div>

				<?php endif; ?>

			<div class="template_spacer"></div>

			<?php if($this->countModules('template-ticker')) : ?>

				<div id="template_ticker">				

					<jdoc:include type="modules" name="template-ticker" style="none" />

				</div>

			<?php endif; ?>				

			<?php if($this->countModules('template-breadcrumb')) : ?>

				<div id="template_breadcrumb">				

					<jdoc:include type="modules" name="template-breadcrumb" style="none" />

				</div>

			<?php endif; ?>

			<?php if($this->countModules('kunena_menu')) : ?>

				<div id="kunena_menu">				

					<jdoc:include type="modules" name="kunena_menu" style="none" />

				</div>

			<?php endif; ?>
							

			<div id="template_container<?php if($this->params->get('content_bg')=='wihoutbar') : echo '_nobar'?><?php endif; ?>">		

				<div id="template_content<?php if($this->params->get('content_bg')=='wihoutbar') : echo '_nobar'?><?php endif; ?>">

					<?php 

						$messages = JFactory::getApplication()->getMessageQueue();

					?>

					<?php if (is_array($messages) && !empty($messages)) : ?>										

						<jdoc:include type="message" />

					<?php endif; ?>

					<jdoc:include type="component" />

					<?php if($this->countModules('template-all-news')) : ?>
						<jdoc:include type="modules" name="template-all-news" style="xhtml" />
		        	<?php endif; ?>

					<div style="float: left;">
					<?php if($this->countModules('template-home-1')) : ?>
						<div id="template-home-1">
						<jdoc:include type="modules" name="template-home-1" style="xhtml" />
						</div>
		        	<?php endif; ?>	
					<?php if($this->countModules('template-home-2')) : ?>
						<div id="template-home-2">
						<jdoc:include type="modules" name="template-home-2" style="xhtml" />
						</div>
		        	<?php endif; ?>	
					</div>
					<div style="float: left;">
					<?php if($this->countModules('template-home-3')) : ?>
						<div id="template-home-3">
						<jdoc:include type="modules" name="template-home-3" style="xhtml" />
						</div>
		        	<?php endif; ?>	
					<?php if($this->countModules('template-home-4')) : ?>
						<div id="template-home-4">
						<jdoc:include type="modules" name="template-home-4" style="xhtml" />
						</div>
		        	<?php endif; ?>	
					</div>


					<?php if($this->countModules('template-social')) : ?>						

						<div id="social">

							<jdoc:include type="modules" name="template-social" style="xhtml" />

						</div>

					<?php endif; ?>

				</div>

<?php if($this->params->get('content_bg')!='wihoutbar') : ?>												

				<div id="template_sidebar_right">					

					<?php if($this->countModules('template-sidebar-right')) : ?>

	        			<jdoc:include type="modules" name="template-sidebar-right" style="sidebarcontent" />

					<?php endif; ?>	

				</div>

<?php endif; ?>

			</div>

			

				<?php if($this->countModules('template-tripletleft-bottom') 

					or $this->countModules('template-tripletmiddle-bottom') 

					or $this->countModules('template-tripletright-bottom')) : ?>	

				

					<div class="template_spacer"></div>	

								

					<div id="template_triplet-bottom">

						<div class="tripletleft">

							<?php if($this->countModules('template-tripletleft-bottom')) : ?>

								<jdoc:include type="modules" name="template-tripletleft-bottom" style="tripletmodule" />

				        	<?php endif; ?>

				       	</div>

				       	<div class="tripletmiddle">

							<?php if($this->countModules('template-tripletmiddle-bottom')) : ?>

								<jdoc:include type="modules" name="template-tripletmiddle-bottom" style="tripletmodule" />

				        	<?php endif; ?>	

				        </div>			    		        

						<div class="tripletright">

					        <?php if($this->countModules('template-tripletright-bottom')) : ?>

								<jdoc:include type="modules" name="template-tripletright-bottom" style="tripletmodule" />

					        <?php endif; ?>

				        </div>

					</div>

				<?php endif; ?>

			

			<div class="template_spacer"></div>

			

			<div id="template_footer">

				<?php if($this->countModules('template-footer')) : ?>

					<jdoc:include type="modules" name="template-footer" style="xhtml" />

				<?php else: ?>				

					<div id="template_copyright">

						&copy;<?php echo date('Y'); ?> <?php echo $app->getCfg('sitename'); ?>

					</div>

					<div id="template_bottommenu">

						<?php if($this->countModules('template-bottommenu')) : ?>

							<jdoc:include type="modules" name="template-bottommenu" style="xhtml" />

						<?php endif; ?>

					</div>

				<?php endif; ?>

			</div>

		</div>
<div style="clear:both;"></div>
		<jdoc:include type="modules" name="template-debug" />

	</body>

</html>

