<?php
/**
 * @version     $Id: item.php 1709 2012-10-06 01:46:10Z joomlaworks $
 * @package     K2
 * @author      JoomlaWorks http://www.joomlaworks.net
 * @copyright   Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license     GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<?php if(JRequest::getInt('print')==1): ?>
<!-- Print button at the top of the print page only -->
<a class="itemPrintThisPage" rel="nofollow" href="#" onclick="window.print();return false;">
    <span><?php echo JText::_('K2_PRINT_THIS_PAGE'); ?></span>
</a>
<?php endif; ?>

 <?php
 if($_POST["seminar"] != NULL || $_POST["seminar"] != "" ||
        $_POST["surname"] != NULL || $_POST["surname"] != "" ||
        $_POST["name"] != NULL || $_POST["name"] != "" ||
        $_POST["category"] != NULL || $_POST["category"] != "" ||
        $_POST["address"] != NULL || $_POST["address"] != "" ||
        $_POST["city"] != NULL || $_POST["city"] != "" ||
        $_POST["pc"] != NULL || $_POST["pc"] != "" ||
        $_POST["fphone"] != NULL || $_POST["fphone"] != "" ||
        $_POST["mphone"] != NULL || $_POST["mphone"] != "" ||
        $_POST["email"] != NULL || $_POST["email"] != ""){
        $session = JFactory::getSession();
        $session->set('price', $_POST['price']);
        $session->set('seminar', $_POST['seminar']);
        $session->set('surname', $_POST['surname']);
        $session->set('address', $_POST['address']);
        $session->set('pc', $_POST['pc']);
        $session->set('mphone', $_POST['mphone']);
        $session->set('name', $_POST['name']);
        $session->set('category', $_POST['category']);
        $session->set('city', $_POST['city']);
        $session->set('fphone', $_POST['fphone']);
        $session->set('email', $_POST['email']);
        $session->set('MerchantCode', $_POST['MerchantCode']);
        $session->set('CardHolderName', $_POST['CardHolderName']);
        $session->set('Installments', $_POST['Installments']);
        $session->set('Charge', $_POST['Charge']);
        $session->set('CurrencyCode', $_POST['CurrencyCode']);

        $price = $session->get('price');
        $seminar3 = $session->get('seminar');
        $surname = $session->get('surname');
        $category = $session->get('category');
        $address = $session->get('address');
        $pc = $session->get('pc');
        $mphone = $session->get('mphone');
        $name = $session->get('name');
        $city = $session->get('city');
        $fphone = $session->get('fphone');
        $email = $session->get('email');}
        $optionArray = $seminar3;
        $number =count($optionArray);
        ?>
<!-- Start K2 Item Layout -->
<span id="startOfPageId<?php echo JRequest::getInt('id'); ?>"></span>

<div id="k2Container_1" class="itemView<?php echo ($this->item->featured) ? ' itemIsFeatured' : ''; ?><?php if($this->item->params->get('pageclass_sfx')) echo ' '.$this->item->params->get('pageclass_sfx'); ?>" style="margin-top:10px;">
<?php
//TRAPEZA
if($_POST["payment"] == "bank"){
    ?>
    <?php
    $session = JFactory::getSession();
    $price = $session->get('price');
        $seminar3 = $session->get('seminar');
        $surname = $session->get('surname');
        $category = $session->get('category');
        $address = $session->get('address');
        $pc = $session->get('pc');
        $mphone = $session->get('mphone');
        $name = $session->get('name');
        $city = $session->get('city');
        $fphone = $session->get('fphone');
        $email = $session->get('email');

        $optionArray = $seminar3;
            $number =count($optionArray);
            if($number>1){
                $seminar=$optionArray[0];
                for ($i=1; $i<$number; $i++) {
                    $seminar .=', '. $optionArray[$i];
                    }
                }
        else $seminar=$optionArray[0];
    ?>
    <div style="text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-style:italic; color:#696969;">
    <div style="height:152px;"></div>
    <div style="width:889px; margin:0 auto; padding:20px 0;">
        <img src="images/seminars/thanks.png" />
        <p style="margin:20px;">Οι τραπεζικοί λογαριασμοί στους οποίους μπορείτε να καταθέσετε τα δίδακτρα (σύνολο: <?php echo $price; ?> Ευρώ) είναι οι ακόλουθοι:</p>
        <div style="width:405px; height:160px; margin:20px auto; padding-bottom: 20px; background-color: #f6f6f6;
            -webkit-box-shadow: 2px 8px 16px 1px rgba(0,0,0,0.25);
            -moz-box-shadow: 2px 8px 16px 1px rgba(0,0,0,0.25);
            box-shadow: 2px 8px 16px 1px rgba(0,0,0,0.25);
            border-radius: 0.5rem">
            <div style="display:block;font-style:normal; font-size:16px; font-weight:bold; color:#0086c5; padding-top:10px;">
            <br/>ΤΡΑΠΕΖΑ ΠΕΙΡΑΙΩΣ<br/><br/>
            <span style="display:block;padding-top:5px">ΙΒΑΝ: GR07 0172 0940 0050 9407 0126 381</span>
            </div>
            <div style="display:block;font-style:normal; font-size:16px; font-weight:bold; color:#0086c5; padding-top:10px;">
            <br/>ALPHA BANK<br/><br/>
            <span style="display:block;padding-top:5px">ΙΒΑΝ: GR60 0140 2190 2190 0232 0001 467</span>
            </div>
        </div>
        <p style="width:100%; text-align:justify;">Στο καταθετήριο της τράπεζας παρακαλώ αναγράψετε ως αιτιολογία το πλήρες ονοματεπώνυμο σας καθώς και <?php if(count($seminar3)>1) echo "τα σεμινάρια"; else echo "το σεμινάριο"; ?> (<?php echo $seminar; ?>) που έχετε επιλέξει.<br />
Εφόσον κάνετε την κατάθεση, παρακαλούμε να μας στείλετε το αντίγραφο τραπεζικής εντολής <br />
        <span style="display:block; width:525px; margin:30px auto;text-align:center; font-size:14px; color:#4aa7d3; font-weight:bold;">στο email: iekemtee@dga.gr ή να το αποστείλετε στο fax: 2107292666</span>
        <p style="width:100%; text-align:justify;">Με την ταυτοποίηση της πληρωμής, θα  αποσταλλούν στο email που έχετε δηλώσει κατά την εγγραφή σας,
τα στοιχεία πρόσβασής σας στην εκπαιδευτική πλατφόρμα.<br /><br /><br /><br />
Σας ευχόμαστε ΚΑΛΗ ΑΡΧΗ και είμαστε σίγουροι ότι θα πάρετε σημαντικές γνώσεις υψηλού επιπέδου, οι οποίες θα σας βοηθήσουν σημαντικά στην επαγγελματική ή ακαδημαϊκή σας καριέρα και σταδιοδρομία.<br />

Παραμένουμε στη διάθεσή σας για οποιαδήποτε διευκρίνιση ή πληροφορία.</p>
        </p>


    </div>
</div>

<?php
//PISTOTIKI
} elseif($_GET["success"] == "true" && $_GET["payment"] == "credit"){
?>
<?php
    $session = JFactory::getSession();
    $price = $session->get('price');
        $seminar3 = $session->get('seminar');
        $surname = $session->get('surname');
        $category = $session->get('category');
        $address = $session->get('address');
        $pc = $session->get('pc');
        $mphone = $session->get('mphone');
        $name = $session->get('name');
        $cert = $session->get('cert');
        $city = $session->get('city');
        $fphone = $session->get('fphone');
        $email = $session->get('email');
        $id=$session->get('MerchantCode');
        $optionArray = $seminar3;
        $number =count($optionArray);

    ?>
<div style="text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-style:italic; color:#696969;">
    <div style="height:152px;"></div>
    <div style="width:889px; margin:0 auto; padding:20px 0;">
      <form name="form2" action="http://www.dga.gr/paycenter/pay3.php" enctype="multipart/form-data"
                              method="post"  >
        <fieldset class="hidden" style="border:none;" >
            <input type="hidden" name="first_name" value="<?php echo $name;?>" />
            <input type="hidden" name="last_name" value="<?php echo $surname;?>" />
            <input type="hidden" name="email" value="<?php echo $email;?>" />
            <input type="hidden" name="lesson" value="<?php echo $seminar3?>" />
            <input type="hidden" name="installments" value="<?php echo $session->get('Installments');?>" />
        </fieldset>
      </form>

      <?php /*
        <form name="form2" action="https://www.deltapay.gr/entry.asp" enctype="multipart/form-data"
                                method="post"  >
        <fieldset class="hidden" style="border:none;" >


            <input type="hidden" name="MerchantCode" value="<?php echo $id; ?>" />
            <input type="hidden" name="Charge" id="Charge" value="<?php echo $price; ?>" />
            <input type="hidden" name="CardHolderName" id="name" value="<?php echo $session->get('CardHolderName'); ?>" />
            <input type="hidden" name="Installments" value="<?= $session->get('Installments'); ?>">
            <input type="hidden" name="Param1" value="Client Name">
            <input type="hidden" name="Param2" value="IEKEM TEE">
            <input type="hidden" name="CurrencyCode" value="978">
            <input type="hidden" class="textfield" name="CardHolderEmail" id="email" value="<?php echo $email; ?>" />
            <input type="hidden" name="TransactionType" value="1">
            <input type="submit" name="submit" class="buttonPayment"  value="Επιβεβαίωση Πληρωμής" />
        </fieldset>
        </form>
        */?>
        <p>

        </p>
        <p style="width:100%; text-align:justify;">
        </p>


    </div>
    </div>

    <?php

    //header("Refresh: 9; url=http://www..gr/");
} else {
    // display form
    if($_POST["seminar2"] == NULL || $_POST["seminar2"] == "" ||
        $_POST["surname2"] == NULL || $_POST["surname2"] == "" ||
        $_POST["name2"] == NULL || $_POST["name2"] == "" ||
        $_POST["category2"] == NULL || $_POST["category2"] == "" ||
        $_POST["address2"] == NULL || $_POST["address2"] == "" ||
        $_POST["city2"] == NULL || $_POST["city2"] == "" ||
        $_POST["pc2"] == NULL || $_POST["pc2"] == "" ||
        $_POST["fphone2"] == NULL || $_POST["fphone2"] == "" ||
        $_POST["mphone2"] == NULL || $_POST["mphone2"] == "" ||
        $_POST["email2"] == NULL || $_POST["email2"] == ""){
    ?>
        <?php

        include("feedback.php");



        /*// retrieve data from database
        $con = mysql_connect($host, $user, $password) or
            die("<br /><br />H σύνδεση με το server απέτυχε<br /><br />");

        mysql_select_db($db, $con) or
            die("<br /><br />H σύνδεση με τη βάση απέτυχε<br /><br />");

        // set encoding
        mysql_query("SET NAMES 'utf8';", $con);
        mysql_query("SET CHARACTER SET 'utf8';", $con);

        /* first the seminar
        $query = "SELECT * FROM seminar WHERE SEMINARID = " . $_POST["seminar"] . ";";
        $result = mysql_query($query, $con);
        $row = mysql_fetch_array($result);
        if (isset($_POST['seminar'])) {
            $optionArray = $_POST['seminar'];
            $number =count($optionArray);
            if($number>1){
                $seminar=$optionArray[0];
                for ($i=1; $i<$number; $i++) {
                    $seminar .=', '. $optionArray[$i];
                    }
                }
        }else $seminar=$optionArray[0];
        //$seminar = $_POST["seminar"];*/

        // material language


        // education level


        // english level

    $session = JFactory::getSession();
    $price = $session->get('price');
        $seminar2 = $session->get('seminar');
        $surname = $session->get('surname');
        $category = $session->get('category');
        $address = $session->get('address');
        $pc = $session->get('pc');
        $mphone = $session->get('mphone');
        $name = $session->get('name');
        $cert = $session->get('cert');
        $city = $session->get('city');
        $fphone = $session->get('fphone');
        $email = $session->get('email');
        $optionArray = $seminar2;
        $number =count($optionArray);
        $seminar = (implode('|',$seminar2));






        $db =JFactory::getDBO();


        $query = "INSERT INTO `jos_candidate` (`name`,`surname`,`city`,`email`,`seminars`)
        VALUES ('".$name."','".$surname."','".$city."','".$email."','".$seminar."')";
        $db->setQuery($query);
        $db->query();




        $candidate = array("seminar" => $seminar,
                            "number" => $number,
                            "surname" => $surname,
                            "name" => $name,
                            "category" => $category,
                            "address" => $address,
                            "city" => $city,
                            "pc" => $pc,
                            "fphone" => $fphone,
                            "mphone" => $mphone,
                            "email" => $email);
        $payment=$_POST["payment"];


        // send email to medialab
        if($_POST["email"] != NULL || $_POST["email"] != "")
        sendMailToMedialab($candidate);

        // send email to candidate
        //sendMailToCandidate($surname, $seminar, $sendTo);
        //header("Location: index.php/register-step2?success=true&payment=$payment");


        ?>

        <form name="form2" action="http://www.dga.gr/paycenter/pay3.php" method="post" style="padding:40px 0 20px 0;" onsubmit="return validateForm2();">

            <fieldset class="hidden" style="display:none;">
            <input type="hidden" name="MerchantCode" value="3558" >
                <input type="hidden" name="Charge" id="charge" value="<?php echo $price; ?>" />
                <input type="hidden" name="TransactionType" value="1">

                <input type="hidden" name="CurrencyCode" value="978">
                <input type="hidden" name="CardHolderEmail" id="email" value="<?php echo $email; ?>" />

            </fieldset>
            <fieldset class="unit"  style="border:none;padding:0; margin:0;">
            <legend style="display:block; width:935px; height:60px;margin-left: -3px; background:url(images/seminars/enroll_head.png) no-repeat;"><span style="display:block;margin-left:90px; padding-top:18px; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; color:#5f6a72;">Τρόποι πληρωμής</span></legend>

            <div style="width:889px; height:46px; margin:0 auto; padding-top:20px;">
            <div id="leftcol" style="display:inline-block; width:440px; float:left;">
                <select id="payment" name="payment" onchange="check(this)" style="width:100%; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:14px; color:#797979; padding-left: 20px; border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1); ">
                    <option value="credit">Πληρωμή μέσω Πιστωτικής κάρτας</option>
                    <option value="bank">Πληρωμή μέσω κατάθεσης σε Τραπεζικό Λογαριασμό</option>


                </select>
            </div>

            <div id="rightcol" style="display:inline-block; width:440px; float:right;">
                <select name="Installments" id="installments" style="width:100%; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:14px; color:#797979; padding-left:20px; border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);">
                    <option id="doseis" value="1" selected="selected">Αριθμός Άτοκων Δόσεων</option>
                    <option value="1">Χωρίς δόσεις</option>
                    <?php if($price<=1000) {$i=2; while($i<=6){echo '<option value="'.$i.'">'.$i.'</option>'; $i++;}}
                    else {$i=2; while($i<=12){echo '<option value="'.$i.'">'.$i.'</option>';$i++;}} ?>

                  </select>
            </div>
            </div>


            <div id="info" style="width:889px; margin:0 auto; padding:10px 0; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:12px; color:#696969; text-align:center;"><img src="images/seminars/cards.png" />
            </div>
            </fieldset>
             <fieldset class="hidden" style="display:none;">
            <input type="hidden" name="seminar2" id="seminar2" value="<?php
                $seminar = implode('|',$seminar2);
                echo $seminar2; ?>"/>

                <input type="hidden" class="textfield" name="surname2" id="surname" value="<?php echo $surname; ?>"/>
                <input type="hidden" class="textfield" name="fname2" id="fname" value="<?php echo $fname; ?>" />
                <input type="hidden" class="textfield" name="address2" id="address" value="<?php echo $address; ?>" />
                <input type="hidden" class="textfield" name="pc2" id="pc" value="<?php echo $pc; ?>" />
                <input type="hidden" class="textfield" name="name2" id="name" value="<?php echo $name; ?>" />
                <input type="hidden" class="textfield" name="category2" id="category" value="<?php echo $category; ?>" />
                <input type="hidden" class="textfield" name="city2" id="city" value="<?php echo $city; ?>" />
                <input type="hidden" class="textfield" name="fphone2" id="fphone" value="<?php echo $fphone; ?>" />
                <input type="hidden" class="textfield" name="mphone2" id="mphone" value="<?php echo $mphone; ?>" />
                <input type="hidden" class="textfield" name="email2" id="email" value="<?php echo $email; ?>" />
            </fieldset>

            <fieldset class="unit" style="border:none;padding:0; margin:0;">
            <legend style="display:block; width:935px; height:60px;margin-left: -3px; background:url(images/seminars/enroll_head.png) no-repeat;"><span style="display:block;margin-left:90px; padding-top:18px; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; color:#5f6a72;">Φορολογικά στοιχεία</span></legend>
            <div id="invoice"  style="width:889px; margin:0 auto; padding:20px 0;font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:12px; color:#696969;">
            Εαν επιθυμείτε την έκδοση <span style="color:#4aa7d3;">τιμολογίου</span> για την επιχείρησή σας, παρακαλούμε συμπληρώστε τα απαραίτητα στοιχεία στα πεδία που ακολουθούν. Σε περίπτωση που επιθυμείτε την έκδοση απόδειξης βάσει των προσωπικών σας στοιχείων, συμπληρώστε το ΑΦΜ και την ΔΟΥ στην οποία ανήκετε.
            </div>

            <div style="width:889px; margin:0 auto; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:14px; color:#414141; margin-bottom:20px;">
            <div id="leftcol" style="display:inline-block; width:389px; float:left;margin-left: 20px;">
                <label for="company">Επωνυμία Επιχείρησης</label><br />
                <input type="text" class="textfield" name="company" id="company" size="25" tabindex="17" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" /><br />
                <label for="vat">Α.Φ.Μ. <span class="required2"><sup>*</sup></span></label><br />
                <input type="text" class="textfield" name="Param1" id="Param1" size="25" tabindex="19" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" /><br />
                <label for="caddress">Διεύθυνση Επιχείρησης</label><br />
                <input type="text" class="textfield" name="caddress" id="caddress" size="25" tabindex="21" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" />
            </div>
            <div id="rightcol" style="display:inline-block; width:389px; float:right;margin-right: 20px;">
                <label for="activity">Δραστηριότητα</label><br />
                <input type="text" class="textfield" name="activity" id="activity" size="25" tabindex="18" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" /><br />
                <label for="doy">Δ.Ο.Υ. <span class="required2"><sup>*</sup></span></label><br />
                <input type="text" class="textfield" name="Param2" id="Param2" size="25" tabindex="20" style="width: 100%;border: 1px solid #c4cbc7;border-radius: 3px;-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);-moz-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);" /><br />
            </div>
            </div>
            </fieldset>
            <legend style="display:block; width:935px; height:60px;margin-left: -3px; background:url(images/seminars/enroll_head.png) no-repeat;"><span style="display:block;margin-left:90px; padding-top:18px; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; color:#5f6a72;">Όροι Χρήσης:</span></legend>
            <div id="agreement" style="width:889px; margin:0 auto;font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; padding:20px 0;">

            <div id="oroi" style="text-align:justify; width:889px; margin:0 auto;font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;">


                Η χρήση του προσωπικού σας λογαριασμού (login, password) είναι αυστηρώς προσωπική και
                <span class="bold">ΔΕΝ ΕΠΙΤΡΕΠΕΤΑΙ</span>  η μεταβίβασή του σε τρίτο άτομο, ούτε η χρησιμοποίησή
                του από άτομο άσχετο με τον εγγεγραμμένο στο σεμινάριο. Όλες οι προσβάσεις των χρηστών
                παρακολουθούνται συνεχώς (log files) για λόγους ασφαλείας, και αν διαπιστωθεί οποιαδήποτε παράβαση, ο
                χρήστης διαγράφεται αυτομάτως από το σύστημα και αποβάλλεται από την συνέχεια του σεμιναρίου.<br/><br/>
                Η χρήση του κωδικού εγγραφής του μαθήματος (Key) είναι αυστηρώς προσωπική για τον
                κάθε χρήστη και <span class="bold">ΑΠΑΓΟΡΕΥΕΤΑΙ</span> η <span class="bold">ΜΕΤΑΒΙΒΑΣΗ</span> του
                σε τρίτο άτομο μη εγεγγραμένο. Οποιαδήποτε είσοδος ατόμου που δεν έχει εγγραφεί στο μάθημα
                χρησιμοποιώντας το κλειδί, αυτομάτως σημαίνει ότι ο κωδικός εγγραφής υποκλάπηκε ή διαδόθηκε
                από τον χρήστη, οπότε ο κωδικός εγγραφής θα αλλάξει αμέσως και οι παράνομοι χρήστες θα
                αποβληθούν από την συνέχεια του σεμιναρίου.<br/><br/>
                Από την στιγμή της καταβολής των διδάκτρων, τα χρήματα <span class="bold">ΔΕΝ ΕΠΙΣΤΡΕΦΟΝΤΑΙ</span>,
                εκτός της περίπτωσης πλήρους αναβολής των μαθημάτων, με ευθύνη του ΙΕΚΕΜ. Οποιοσδήποτε χρήστης αδυνατεί
                να παρακολουθήσει το σεμινάριο λόγω <span class="bold">ΣΟΒΑΡΟΥ</span> κωλύματος ενώ έχει καταβάλλει
                τα δίδακτρα, δύναται μέσω επικοινωνίας με τους υπεύθυνους του προγράμματος να μεταφερθεί σε επόμενη
                περίοδο διδασκαλίας, έτσι ώστε να ολοκληρώσει το πρόγραμμα εκπαίδευσης.

            </div>
            <br />
            <hr style="background: #ebf1f4;" />
            Για να αποστείλετε την αίτησή σας θα πρέπει να διαβάσετε προσεκτικά τους όρους χρήσης, να τσεκάρετε το
            check-box που ακολουθεί και να πατήσετε το κουμπί "Πληρωμή". Τσεκάροντας το check-box βεβαιώνετε ότι:
            <br /><br />
                Επιθυμείτε να παρακολουθήσετε το σεμινάριο που επιλέξατε<br />
                Αποδέχεστε πλήρως τους όρους χρήσης<br />
                Τα στοιχεία που συμπληρώσατε είναι αληθή<br />
            <br />
            </div>
            <div id="acceptance" style="text-align:center; font-size:14px; color:#4aa7d3; padding-bottom:20px;">
                <label style="font-weight:normal;" for="accept">Δηλώνω υπεύθυνα πως τα στοιχεία που υποβάλλω είναι αληθή και αποδέχομαι πλήρως τους όρους χρήσης:</label>
                <input type="checkbox" name="accept" id="accept" tabindex="20" />
            </div>
            <input type="hidden" id="first_namepay" class="form_field" type="text" name="first_namepay" value="<?php echo $name; ?>"/>
            <input type="hidden" id="last_name" class="form_field" type="text" name="last_name" value="<?php echo $surname; ?>"/>
            <input type="hidden" id="phone_number" class="form_field" type="text" name="phone_number" value="<?php echo $fphone; ?>"/>
            <input type="hidden" id="email" class="form_field" type="text" name="email" value="<?php echo $email; ?>"/>
            <input type="hidden" id="lessonsname" class="form_field" type="text" name="lessonsname" value="<?php foreach($seminar3 as $seminar => $value) { echo $value .','; } ?>"/>
            <input type="hidden" id="installments" class="form_field" type="text" name="installments" value="<?php echo $session->get('Installments');?>"/>
            <input type="hidden" name="charging" id="charge" value="<?php echo $price; ?>" />
            <div id="buttons" style="text-align:center;">
            <input type="reset" class="cbutton" value="Καθαρισμός" tabindex="28" style="width:183px; height:41px; background:url(images/seminars/payment.png) no-repeat; border:none; cursor:pointer; font-family:'Century Gothic'; font-size:16px; color:#666666; font-weight:bold;"/>
            <input type="submit" class="cbutton" value="Πληρωμή" tabindex="27" style="width:183px; height:41px; background:url(images/seminars/payment.png) no-repeat; border:none; cursor:pointer; font-family:'Century Gothic'; font-size:16px; color:#666666; font-weight:bold;"/>

            </div>
        </form>

         <div id="contact_1" style="width:940px; height:186px; margin-left: -2px; margin-top:10px; background:url(images/seminars/footer.png);">
        <div style="width:50%; float:left; margin-top:58px; margin-left:21px;">
       <span style="display:inline-block;"><a onclick="window.open('https://services.yuboto.com/click2call/new/c2t.aspx?id=6B87B610-D272-4E1E-BD32-B961B6573D2E','Image','scrollbars=no,toolbar=no,location=no,status=no,resizable=no,screenX=136,screenY=83');return false;" href="https://services.yuboto.com/click2call/new/c2t.aspx?id=6B87B610-D272-4E1E-BD32-B961B6573D2E" class="TT-container">
       <img src="images/seminars/click2call_1.png" border="0" onmouseover="this.src='images/seminars/click2call_2.png'" onmouseout="this.src='images/seminars/click2call_1.png'">
    <span class="TT-value"></span></a></span>
    <span style="display:inline-block; margin-left:16px; position:relative; top:-13px;">
        <span style="color:#67348e; font-size:14px; font-weight:bold;"></span>
        <br />
        <span style="width:259px; color:#555555; font-size:14px; font-weight:bold; position:relative; top:-5px;">Για να συνομιλήσετε μαζί μας χωρίς χρέωση<br />άμεσα, πατήστε εδώ.</span>
    </span>

        </div>

        <div style="display:inline-block; float:right; margin-top:52px; margin-right:21px;"">
            <span style="display:block;float:right; font-style:italic; color:#262626; font-size:14px;">
            Για περισσότερες πληροφορίες σχετικά με τα σεμινάρια μας:
            </span>
            <br /><br />
            <span style="margin-top:20px;">
             <span style="display:inline-block; float:right; ">
            <a href="http://www.youtube.com/user/elearnDigitalAcademy" target="_blank">
            <img src="images/seminars/youtube.png" />
            </a>
            <a href="https://www.facebook.com/dga.gr" target="_blank">
            <img src="images/seminars/facebook.png" />
            </a>
            </span>
            <span style="display:inline-block;float:right; font-weight:bold; color:#0086c5; font-size:18px; margin-right:20px;margin-top: 5px; text-align:right;">
            τηλ: 210 3003536<br />Email: <a href="mailto:iekemtee@dga.gr" target="_blank" style="font-weight:bold; color:#0086c5; font-size:18px; text-decoration:none;">iekemtee@dga.gr</a>

            </span>
           </span>
        </div>
    </div>

        <?php
    } else {
        include("feedback.php");
        ?>

        <?php
        /*$sendTo = trim($_POST["email"]);
        //check if the email address is invalid

        //if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $sendTo)) {
        //    die("<br /><br />To email που δώσατε δεν είναι έγκυρο.<br /><br />");
        //}

        // check spam
        $mailcheck = spamcheck($sendTo);
        if ($mailcheck == false){
            die("<br /><br />To email που δώσατε δεν είναι έγκυρο.<br /><br />");
        }

        // retrieve data from database
        $con = mysql_connect($host, $user, $password) or
            die("<br /><br />H σύνδεση με το server απέτυχε<br /><br />");

        mysql_select_db($db, $con) or
            die("<br /><br />H σύνδεση με τη βάση απέτυχε<br /><br />");

        // set encoding
        mysql_query("SET NAMES 'utf8';", $con);
        mysql_query("SET CHARACTER SET 'utf8';", $con);

        /* first the seminar
        $query = "SELECT * FROM seminar WHERE SEMINARID = " . $_POST["seminar"] . ";";
        $result = mysql_query($query, $con);
        $row = mysql_fetch_array($result);*/

        //$seminar = $_POST["seminar"];

        // material language


        // education level


        // english level
        $session = JFactory::getSession();
        $price = $session->get('price');
        $seminar = $session->get('seminar');
        $surname = $session->get('surname');
        $category = $session->get('category');
        $address = $session->get('address');
        $pc = $session->get('pc');
        $mphone = $session->get('mphone');
        $name = $session->get('name');
        $city = $session->get('city');
        $fphone = $session->get('fphone');
        $email = $session->get('email');
        $optionArray = $seminar;
        $number =count($optionArray);
        $seminar = (implode('|',$seminar));




        /* create new record for registered candidate

        $insert = "INSERT INTO candidate (SURNAME, NAME, FNAME, CERTIFICATE,
                ADDRESS, CITY, PC, FPHONE, MPHONE, EMAIL)
                VALUES ('" . $_POST["surname"] . "', '" .
                $_POST["name"] . "', '" . $_POST["fname"] . "', '" .
                $_POST["cert"] . "', '" . $_POST["address"] . "', '" .
                $_POST["city"] . "', '" . $_POST["pc"] . "', '" .
                $_POST["fphone"] . "', '" . $_POST["mphone"] . "', '" .
                $sendTo . "');";


        // set the encoding
        mysql_query("SET NAMES 'utf8';", $con);
        mysql_query("SET CHARACTER SET 'utf8';", $con);

        // execute insert
        if (!mysql_query($insert, $con))
            die("Σφάλμα:" . mysql_error($con));

        // close connection
        mysql_close($con);*/

        $candidate = array("seminar" => $seminar,
                            "number" => $number,
                            "language" => $language,
                            "surname" => $surname,
                            "name" => $name,
                            "category" => $category,
                            "address" => $address,
                            "city" => $city,
                            "pc" => $pc,
                            "fphone" => $fphone,
                            "mphone" => $mphone,
                            "email" => $email,
                            "company" => $_POST["company"],
                            "activity" => $_POST["activity"],
                            "vat" => $_POST["vat"],
                            "doy" => $_POST["doy"],
                            "caddress" => $_POST["caddress"]);
        $payment=$_POST["payment"];
                $session = JFactory::getSession();
                $session->set('MerchantCode', $_POST['MerchantCode']);
        $session->set('CardHolderName', $_POST['CardHolderName']);
        $session->set('Installments', $_POST['Installments']);
        $session->set('Charge', $_POST['Charge']);
        $session->set('CurrencyCode', $_POST['CurrencyCode']);
        $session->set('TransactionType', $_POST['TransactionType']);


        // upload resume
        //uploadFile();

        // send email to medialab
        if($_POST["email"] != NULL || $_POST["email"] != ""){sendMailToMedialab2($candidate);}
        $instance =& JURI::getInstance();
        $url1 = JURI::getInstance()->toString();
        $url = strtok($url1, '?');


        // send email to candidate
        //sendMailToCandidate($surname, $seminar, $sendTo);
        header("Location: $url?success=true&payment=$payment");


        ?>

        <?php
    }
}

?>

    <div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
